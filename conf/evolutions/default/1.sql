# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table candidate (
  id                        varchar(255) not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  party                     varchar(255),
  pollstation               varchar(255),
  constraint pk_candidate primary key (id))
;

create table election (
  id                        varchar(255) not null,
  name                      varchar(255),
  starting_time             timestamp,
  length                    bigint,
  open                      boolean,
  constraint pk_election primary key (id))
;

create table party (
  id                        varchar(255) not null,
  name                      varchar(255),
  constraint pk_party primary key (id))
;

create table poll_station (
  id                        varchar(255) not null,
  name                      varchar(255),
  election                  varchar(255),
  constraint pk_poll_station primary key (id))
;

create table vote (
  id                        varchar(255) not null,
  pollstation               varchar(255),
  assigned                  boolean,
  constraint pk_vote primary key (id))
;

create table voter (
  id                        varchar(255) not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  identification            varchar(255),
  eligible                  boolean,
  email                     varchar(255),
  constraint pk_voter primary key (id))
;


create table poll_station_voter (
  poll_station_id                varchar(255) not null,
  voter_id                       varchar(255) not null,
  constraint pk_poll_station_voter primary key (poll_station_id, voter_id))
;
create sequence candidate_seq;

create sequence election_seq;

create sequence party_seq;

create sequence poll_station_seq;

create sequence vote_seq;

create sequence voter_seq;

alter table candidate add constraint fk_candidate_party_1 foreign key (party) references party (id) on delete restrict on update restrict;
create index ix_candidate_party_1 on candidate (party);
alter table candidate add constraint fk_candidate_pollStation_2 foreign key (pollstation) references poll_station (id) on delete restrict on update restrict;
create index ix_candidate_pollStation_2 on candidate (pollstation);
alter table poll_station add constraint fk_poll_station_election_3 foreign key (election) references election (id) on delete restrict on update restrict;
create index ix_poll_station_election_3 on poll_station (election);
alter table vote add constraint fk_vote_pollStation_4 foreign key (pollstation) references poll_station (id) on delete restrict on update restrict;
create index ix_vote_pollStation_4 on vote (pollstation);



alter table poll_station_voter add constraint fk_poll_station_voter_poll_st_01 foreign key (poll_station_id) references poll_station (id) on delete restrict on update restrict;

alter table poll_station_voter add constraint fk_poll_station_voter_voter_02 foreign key (voter_id) references voter (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists candidate;

drop table if exists election;

drop table if exists party;

drop table if exists poll_station;

drop table if exists poll_station_voter;

drop table if exists vote;

drop table if exists voter;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists candidate_seq;

drop sequence if exists election_seq;

drop sequence if exists party_seq;

drop sequence if exists poll_station_seq;

drop sequence if exists vote_seq;

drop sequence if exists voter_seq;

