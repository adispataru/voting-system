package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 6/6/15.
 */
@Entity
public class Voter extends Model{

    @Id
    @GeneratedValue
    private String id;
    private String firstName;
    private String lastName;
    private String identification;
    private boolean eligible;
    @ManyToMany(mappedBy = "voterList",fetch = FetchType.LAZY)
    private List<PollStation> pollStation;
    private String email;

    public static Finder<String, Voter> find = new Finder<>(String.class, Voter.class);

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public boolean isEligible() {
        return eligible;
    }

    public void setEligible(boolean eligible) {
        this.eligible = eligible;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<PollStation> getPollStation() {

        if (pollStation == null)
            pollStation = new ArrayList<>();
        return pollStation;
    }

    public void setPollStation(List<PollStation> pollStation) {
        this.pollStation = pollStation;
    }
}
