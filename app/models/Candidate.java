package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 6/6/15.
 */
@Entity
public class Candidate extends Model{

    @Id
    @GeneratedValue
    private String id;
    private String firstName;
    private String lastName;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "party")
    @JsonIgnore
    private Party party;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pollstation")
    @JsonIgnore
    private PollStation pollStation;

    public static Model.Finder<String, Candidate> find = new Model.Finder<>(String.class, Candidate.class);

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public String getName(){
        return firstName + " " + lastName;
    }

    public PollStation getPollStation() {
        return pollStation;
    }

    public void setPollStation(PollStation pollStation) {
        this.pollStation = pollStation;
    }
}
