package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 6/6/15.
 */
@Entity
public class Party extends Model{

    @Id
    @GeneratedValue
    private String id;
    private String name;
    @OneToMany(mappedBy = "party", cascade = CascadeType.ALL)
    private List<Candidate> candidateList;

    public static Finder<String, Party> find = new Finder<>(String.class, Party.class);

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Candidate> getCandidateList() {
        if (candidateList == null)
            candidateList = new ArrayList<>();
        return candidateList;
    }

    public void setCandidateList(List<Candidate> candidateList) {
        this.candidateList = candidateList;
    }
}
