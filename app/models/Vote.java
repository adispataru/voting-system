package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Created by adispataru on 6/8/15.
 */
@Entity
public class Vote extends Model{

    @Id
    private String id;
    private Candidate candidate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pollstation")
    @JsonIgnore
    private PollStation pollStation;
    private boolean assigned;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public boolean isAssigned() {
        assigned = candidate != null;
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    public PollStation getPollStation() {
        return pollStation;
    }

    public void setPollStation(PollStation pollStation) {
        this.pollStation = pollStation;
    }
}
