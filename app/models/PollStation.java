package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 6/6/15.
 */
@Entity
public class PollStation extends Model{

    @Id
    @GeneratedValue
    private String id;
    @OneToMany(mappedBy = "pollStation", cascade = CascadeType.ALL)
    private List<Candidate> candidateList;
    @ManyToMany
    private List<Voter> voterList;
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "election")
    @JsonIgnore
    private Election election;
    @OneToMany(mappedBy = "pollStation", cascade = CascadeType.ALL)
    private List<Vote> votes;

    public static Model.Finder<String, PollStation> find = new Model.Finder<>(String.class, PollStation.class);

    public List<Voter> getVoterList() {
        if (voterList == null)
            voterList = new ArrayList<>();
        return voterList;
    }

    public void setVoterList(List<Voter> voterList) {
        this.voterList = voterList;
    }

    public List<Candidate> getCandidateList() {
        if (candidateList == null)
            candidateList = new ArrayList<>();
        return candidateList;
    }

    public void setCandidateList(List<Candidate> candidateList) {
        this.candidateList = candidateList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Election getElection() {
        return election;
    }

    public void setElection(Election election) {
        this.election = election;
    }
}
