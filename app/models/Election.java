package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by adispataru on 6/6/15.
 */
@Entity
public class Election extends Model{
    @Id
    @GeneratedValue
    private String id;
    private String name;
    private Date startingTime;
    private long length;
    @OneToMany(mappedBy = "election", cascade = CascadeType.ALL)
    private List<PollStation> pollStationList;


    private boolean open;

    public static Finder<String, Election> find = new Finder<>(String.class, Election.class);



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(Date startingTime) {
        this.startingTime = startingTime;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public boolean isOpen() {
        open = startingTime.getTime() + length < System.currentTimeMillis();
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }


    public List<PollStation> getPollStationList() {
        return pollStationList;
    }

    public void setPollStationList(List<PollStation> pollStationList) {
        this.pollStationList = pollStationList;
    }
}
