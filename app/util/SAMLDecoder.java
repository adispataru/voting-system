package util;

import play.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by adrian on 2/21/15.
 */
public class SAMLDecoder {

    private Map<String, ArrayList<String>> attributes;

    public SAMLDecoder(Map<String, Object> attrs){
        attributes = new HashMap<>();
//        Logger.debug("SAML Attributes");
        for(String s : attrs.keySet()){
            ArrayList<String> values = (ArrayList) attrs.get(s);
            attributes.put(s, values);
//            Logger.debug(s + ": " + values);
        }
    }

    public String getFirst(String key){
        return attributes.get(key).get(0);
    }

    public String getAt(String key, int index){
        return attributes.get(key).get(index);
    }

    public List<String> getList(String key){
        return attributes.get(key);
    }
}
