package notifiers;


import models.Voter;
import play.libs.mailer.Email;
import play.libs.mailer.MailerPlugin;

/**
 * Created by adrian on 2/21/15.
 */
public class Mails {


    public static final String VOTED = "Dear %s, your vote has been registered with id %s.";

        public static void test(){
        Email email = new Email();
        email.setSubject("Voting notification");
        email.setFrom("Voting system  <no-reply@info.uvt.ro>");
        email.addTo("adi.spataru92@gmail.com");
        // sends text, HTML or both...
        email.setBodyText(String.format(VOTED, "John Doe", "ursulpacalitdevulpe"));

        MailerPlugin.send(email);
    }

    public static void notifyVoter(Voter v, String id){
        Email email = new Email();
        email.setSubject("Voting notification");
        email.setFrom("Voting system  <no-reply@info.uvt.ro>");
        email.addTo(v.getEmail());
        // sends text, HTML or both...
        email.setBodyText(String.format(VOTED, v.getFirstName() + " " + v.getLastName(), id));

        MailerPlugin.send(email);
    }

}
