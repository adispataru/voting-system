package controllers;

import models.Election;
import models.PollStation;
import models.Voter;
import org.pac4j.play.java.JavaController;
import org.pac4j.play.java.RequiresAuthentication;
import org.pac4j.saml.profile.Saml2Profile;
import play.mvc.Result;
import util.SAMLDecoder;
import views.html.auth;
import views.html.dashboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by adrian on 11.12.2014.
 * Authorized class. This allows access based on SAML
 */
public class Authorized extends JavaController {

    @RequiresAuthentication(clientName = "Saml2Client")
    public static Result display(){
        Saml2Profile profile = (Saml2Profile) getUserProfile();
        SAMLDecoder decoder = new SAMLDecoder(profile.getAttributes());
        String role;
        if(Admin.isAdmin(profile)){
            role = "admin";
        }else{
            role = "user";
        }

        Map<String, String> args = new HashMap<>();
        args.put("title", "Voting System");
        args.put("role", role);
        args.put("auth", "true");

        Voter u = Voter.find.where().eq("email",
                decoder.getFirst("mail"))
                .findUnique();
        if(u == null){
            args.put("eligible", "false");
        }else {
            args.put("eligible", "true");
        }

        return ok(auth.render(args));
    }

    @RequiresAuthentication(clientName = "Saml2Client")
    public static Result votingDashboard(){
        Saml2Profile profile = (Saml2Profile) getUserProfile();
        SAMLDecoder decoder = new SAMLDecoder(profile.getAttributes());
        Voter v = Voter.find.where().eq("email", decoder.getFirst("mail")).findUnique();
        if(v!= null && !v.isEligible()) {
            return redirect(controllers.routes.Authorized.display());
        }
        Map<String, String> args = new HashMap<>();
        args.put("page", "main");
        args.put("section", "default");
        args.put("title", "Voting System");
        args.put("role", "voter");
        args.put("auth", "true");

        List<Election> elections = Election.find.findList();
        List<Election> eligibleElections = new ArrayList<>();
        for (Election e: elections){
            boolean eligible = false;
            for(PollStation ps : PollStation.find.findList()){
                if (true)
                    eligible = true;
                else{
                   // e.getPollStationList().remove(ps.getId());
                }
            }

            if(eligible){
                eligibleElections.add(e);
            }
        }

        return ok(dashboard.render(args, elections));
    }


}
