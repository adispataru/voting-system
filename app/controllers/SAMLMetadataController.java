package controllers;

import org.pac4j.core.client.Clients;
import org.pac4j.play.Config;
import org.pac4j.play.java.JavaController;
import org.pac4j.saml.client.Saml2Client;
import play.Logger;
import play.mvc.Result;

/**
 * Created by adrian on 3/12/15.
 */
public class SAMLMetadataController extends JavaController {

    public static Result callBack(){
        Clients clients =  Config.getClients();
        Saml2Client saml2Client = (Saml2Client) clients.findClient("Saml2Client");
        if(saml2Client != null) {
            Logger.debug("SAMLMetadataController successfully callback");
            return ok(saml2Client.printClientMetadata()).as("application/xml");
        }
        return badRequest();
    }
}
