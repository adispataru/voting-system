package controllers;

import org.pac4j.play.java.JavaController;
import play.mvc.Result;
import views.html.index;

import java.util.HashMap;
import java.util.Map;

public class Application extends JavaController {


    public static Result index() {
        String role = "user";

        Map<String, String> args = new HashMap<>();
        args.put("title", "Voting System");
        args.put("role", role);
        args.put("auth", "false");

        return ok(index.render(args));
    }

}
