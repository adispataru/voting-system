package controllers;

import models.*;
import notifiers.Mails;
import org.pac4j.play.java.JavaController;
import org.pac4j.play.java.RequiresAuthentication;
import org.pac4j.saml.profile.Saml2Profile;
import play.Logger;
import play.mvc.Result;
import util.SAMLDecoder;

import java.util.List;
import java.util.UUID;

import static play.libs.Json.toJson;

/**
 * Created by adrian on 2/21/15.
 */
public class API extends JavaController {


    public static Result test(){

        List<PollStation> parties = PollStation.find.findList();
        for (PollStation p : parties){
            Logger.debug(p.getName());
            for (Candidate c : Candidate.find.all())
                if (c.getPollStation().getId().equals(p.getId()))
                    Logger.debug(c.getName());
        }

        Logger.debug("-------------^^-------------");
        return ok(toJson(Election.find.findList()));
    }



    @RequiresAuthentication(clientName = "Saml2Client")
    public static Result vote(String candidateId, String pollstationId){
        Candidate c = Candidate.find.where().eq("id", candidateId).findUnique();

        Saml2Profile profile = (Saml2Profile) getUserProfile();
        SAMLDecoder decoder = new SAMLDecoder(profile.getAttributes());

        Voter v = Voter.find.where().eq("email", decoder.getFirst("mail")).findUnique();
        UUID id = UUID.randomUUID();

        if(c != null && v != null){
            PollStation p = PollStation.find.byId(pollstationId);
            if (p != null)
                return badRequest("PollStation id invalid");
            Vote vote = VotePool.getVote(p.getId());
            vote.setCandidate(c);
            vote.save();
            Mails.notifyVoter(v, vote.getId());
            return ok();
        }
        return badRequest("Something went wrong!");
    }


    public static Result getElections(){
        return ok();
    }
    public static Result getElection(String id){
        return ok();
    }
    public static Result getPollStations(String id){
        return ok();
    }
    public static Result getPollStation(String eid, String pid){
        return ok();
    }
    public static Result getPodium(String eid){
        return ok();
    }
    public static Result getTime(String eid){
        return ok();
    }

}
