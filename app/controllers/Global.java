package controllers;

import models.*;
import org.pac4j.core.client.Clients;
import org.pac4j.play.Config;
import org.pac4j.saml.client.Saml2Client;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.Play;
import play.mvc.Action;
import play.mvc.Http;

import java.lang.reflect.Method;
import java.util.Date;

/**
 * Created by adrian on 11.12.2014.
 * Set SAML2 client
 */
public class Global extends GlobalSettings {
    public static long lastUpdate;

    @Override
    public void onStart(final Application app){

        //Config.setErrorPage401(views.html.error401.render().toString());
        //Config.setErrorPage403(views.html.error403.render().toString());


        // Synchronize database
        String[] names = {"Debbie Simpson",
                "Carole Hill",
                "Wendell Bennett",
                "Gary Walsh",
                "Virgil Simmons",
                "Louis Lopez",
                "Marjorie Luna",
                "Ethel Weber",
                "Norman Moody",
                "Peter Blake",
                "Carlos Christensen",
                "Arlene Lee",
                "Ebony Wright"};
        if(Voter.find.findRowCount() == 0) {
            Party[] p = new Party[5];
            p[0] = new Party();
            p[1] = new Party();
            p[2] = new Party();
            p[0].setName("Partidul Florilor");
            p[1].setName("Partidul focului");
            p[2].setName("Partidul bunastarii");
            p[0].save();
            p[1].save();
            p[2].save();
            PollStation ps = new PollStation();
            PollStation ps2 = new PollStation();
            ps.setName("Central");
            ps2.setName("Lake");
            ps.save();
            ps2.save();
            for (int i = 0; i < 10; i++){
                Candidate c = new Candidate();
                c.setFirstName(names[i].split(" ")[0]);
                c.setLastName(names[i].split(" ")[1]);
                c.setParty(p[i%3]);
                c.save();

                if(i % 2 == 0) {
                    c.setPollStation(ps);
                    ps.getCandidateList().add(c);
                }else{
                    c.setPollStation(ps2);
                    ps2.getCandidateList().add(c);
                }
                c.save();
                ps2.save();

                p[i%3].getCandidateList().add(c);
                p[i%3].save();

            }

            Voter v = new Voter();
            v.setEmail("adi.spataru92@gmail.com");
            v.setFirstName("Adrian");
            v.setLastName("Spataru");
            v.setEligible(true);
            v.save();

            Voter v2 = new Voter();
            v2.setEmail("theodor.cernoiu92@e-uvt.ro");
            v2.setFirstName("Theo");
            v2.setLastName("Cernoiu");
            v2.setEligible(true);
            v2.save();


            Election e = new Election();
            e.setStartingTime(new Date());
            e.setLength(3600);
            e.setName("Alegeri randomizate 2015");


            ps.getVoterList().add(v);
            ps2.getVoterList().add(v2);
            ps.save();
            ps2.save();
            e.getPollStationList().add(ps);
            e.getPollStationList().add(ps2);
            e.save();

            for (PollStation s : PollStation.find.where().eq("election", e).findList())
                Logger.debug(s.getId());

            for (Candidate c : Candidate.find.findList())
                Logger.debug(c.getName());


        }

        lastUpdate = System.currentTimeMillis();


        final String baseUrl = Play.application().configuration().getString("baseUrl");


        Config.setDefaultLogoutUrl(baseUrl);

        final Saml2Client saml2Client = new Saml2Client();
        saml2Client.setKeystorePath("resource:mainKeystore.jks");
        saml2Client.setKeystorePassword("a65hgflk");
        saml2Client.setPrivateKeyPassword("a65hgflk");
        saml2Client.setIdpMetadataPath("resource:openidp-feide.xml");
        saml2Client.setCallbackUrl(baseUrl + "callback");

        final Clients clients = new Clients(baseUrl + "callback", saml2Client);
        clients.init();
        Config.setClients(clients);
        Logger.info(new Date().toString());

    }



    public Action onRequest(Http.Request request, Method actionMethod) {

        return super.onRequest(request, actionMethod);
    }

}
