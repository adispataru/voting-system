package controllers;

import org.pac4j.play.java.JavaController;
import org.pac4j.play.java.RequiresAuthentication;
import org.pac4j.saml.profile.Saml2Profile;
import play.mvc.Result;
import util.SAMLDecoder;
import views.html.dashboard;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adrian on 20.02.2015.
 * This controller will display the dashboard with requests to be approved.
 */
public class Admin extends JavaController {
    public static final String AUTH_STRING="adi.spataru92@gmail.com";
    public static final String AUTH_KEY="mail";

    @RequiresAuthentication(clientName = "Saml2Client")
    public static Result display(){
        Saml2Profile profile = (Saml2Profile) getUserProfile();
        SAMLDecoder decoder = new SAMLDecoder(profile.getAttributes());
        if(! isAdmin(profile)) {
            return redirect(controllers.routes.Authorized.display());
        }
        Map<String, String> args = new HashMap<>();
        args.put("page", "main");
        args.put("section", "default");
        args.put("title", "Voting System");
        args.put("role", "admin");
        args.put("auth", "true");
        return ok(dashboard.render(args, null));
    }

    public static boolean isAdmin(Saml2Profile profile){
        SAMLDecoder decoder = new SAMLDecoder(profile.getAttributes());
        if(decoder.getList(Admin.AUTH_KEY).contains(Admin.AUTH_STRING)){
            return true;
        }
        if(decoder.getFirst("urn:oid:1.3.6.1.4.1.5923.1.1.1.6").
                equals("florin.spataru92@e-uvt.ro")){
            return true;
        }
        return false;
    }
}
