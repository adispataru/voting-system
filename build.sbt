name := """euvt-iThenticate"""

version := "0.1"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

resolvers ++= Seq(
  "Sonatype snapshots repository" at "https://oss.sonatype.org/content/repositories/snapshots/",
  "Default Maven Repo" at "https://repo1.maven.org/maven2/"
)

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
  "org.pac4j" % "play-pac4j_java" % "1.3.0",
  "org.pac4j" % "pac4j-saml" % "1.6.0",
  "org.webjars" %% "webjars-play" % "2.3.0-2",
  "org.webjars" % "bootstrap" % "3.1.1-2",
  "mysql" % "mysql-connector-java" % "5.1.18",
  "com.typesafe.play" %% "play-mailer" % "2.4.0"
)

libraryDependencies += "org.apache.xmlrpc" % "xmlrpc-client" % "3.1.3"




