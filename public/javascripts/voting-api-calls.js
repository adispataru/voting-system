function approveRequest(n){
    $.post(
        appContext + "api/approve/" + n,
        function (result){
            $("#user" + n).remove();
        }
    );
}

function rejectRequest(n){
    $.post(
        appContext + "api/reject/" + n,
        function (result){
            $("#user" + n).remove();
        }
    );
}

function deleteRequest(n){
    $.post(
        appContext + "api/delete/" + n,
        function (result){
            $("#user" + n).remove();
        }
    );
}

$(document).ready(function () {
    var userTable = $("#users")
    $.get("/api/elections",
        function(data){
            var table = $("#open");
            for(e in data){

                $.get ("/api/elections/" + e.id, function(data){

                });

                var row = "<tr id='user" + data[user]["id"] + "'>";
                row += "<td>" + data[user]["firstName"] + "</td>";
                row += "<td>" + data[user]["lastName"] + "</td>";
                row += "<td>" + data[user]["email"] + "</td>";
                row += "<td>" + acceptButton + deleteButton + rejectButton + "</td>";
                row += "</tr>";


                var acceptButton = "<a title='Approve' href='javascript:approveRequest(" + data[user]["id"] + ");' class='btn btn-success'>" +
                    "<span class='glyphicon glyphicon-ok' aria-hidden='true'></span></a> ";
                var deleteButton = "<a title='Delete' href='javascript:deleteRequest(" + data[user]["id"] + ");' class='btn btn-warning'>"  +
                    "<span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a> ";
                var rejectButton = "<a title='Block user' href='javascript:rejectRequest(" + data[user]["id"] + ");' class='btn btn-danger'>"  +
                    "<span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></a> ";
                var row = "<tr id='user" + data[user]["id"] + "'>";
                row += "<td>" + data[user]["firstName"] + "</td>";
                row += "<td>" + data[user]["lastName"] + "</td>";
                row += "<td>" + data[user]["email"] + "</td>";
                row += "<td>" + acceptButton + deleteButton + rejectButton + "</td>";
                row += "</tr>";
                table.append(row);
            }
        }
    )

    $.get(appContext +  "api/users",
        function(data){
            for(user in data){
                //var acceptButton = "<a title='Approve' href='javascript:approveRequest(" + data[user]["id"] + ");' class='btn btn-success'>" +
                //    "<span class='glyphicon glyphicon-ok' aria-hidden='true'></span></a> ";
                //var deleteButton = "<a title='Delete' href='javascript:deleteRequest(" + data[user]["id"] + ");' class='btn btn-warning'>"  +
                //    "<span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a> ";
                //var rejectButton = "<a title='Block user' href='javascript:rejectRequest(" + data[user]["id"] + ");' class='btn btn-danger'>"  +
                //    "<span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></a> ";
                var row = "<tr id='user" + data[user]["id"] + "'>";
                row += "<td>" + data[user]["firstName"] + "</td>";
                row += "<td>" + data[user]["lastName"] + "</td>";
                row += "<td>" + data[user]["email"] + "</td>";
                //row += "<td>" + acceptButton + deleteButton + rejectButton + "</td>";
                row += "</tr>";
               userTable.append(row);
            }
        }
    )

});